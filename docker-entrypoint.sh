#!/bin/sh
set -e

cpulimit -l ${CPU_USAGE} -e belli &
/usr/bin/belli -c /etc/confo/config.json
